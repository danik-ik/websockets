package ru.otus.danik_ik.websockets.multiecho;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebSocket
public class MultiEchoWebSocket {
    private Session session;
    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    @OnWebSocketMessage
    public void onMessage(String data) {
        sheduleEcho(data);
    }

    private void sheduleEcho(String data) {
        scheduler.schedule(() -> this.doEcho(data), 1, TimeUnit.SECONDS);
    }

    private void doEcho(String data) {
        try {
            this.getSession().getRemote().sendString(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final String nextData = data.substring(3);
        if (nextData.isEmpty()) return;
        sheduleEcho(nextData);
    }

    @OnWebSocketConnect
    public void onOpen(Session session) {
        setSession(session);
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        // do nothing
    }
}
