var ws;

init = function (path) {
    ws = new WebSocket("ws://localhost:8090/" + path);
    ws.onopen = function (event) {

    }
    ws.onmessage = function (event) {
        var $textarea = document.getElementById("messages");
        $textarea.value = $textarea.value + event.data + "\n";
    }
    ws.onclose = function (event) {

    }
};

function sendMessage() {
    var messageField = document.getElementById("message");
    var message = messageField.value;
    ws.send(message);
    messageField.value = '';
}